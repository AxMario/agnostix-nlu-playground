import tensorflow.python.data
from tensorflow.keras import preprocessing
import tensorflow_text as tf_text
import tensorflow as tf
import numpy as np

"""
see: https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/text_dataset_from_directory
"""


class DatasetLoader:
    def __init__(self, dataset_path):
        self.dataset_path = dataset_path
        self._dataset = None

    def load(self):
        if self._dataset is None:
            self._dataset = preprocessing.text_dataset_from_directory(self.dataset_path,
                                                                      labels='inferred',
                                                                      label_mode='int', # or categorical
                                                                      class_names=['inform', 'request', 'other'],
                                                                      batch_size=1,
                                                                      shuffle=True
                                                                      )

    @property
    def dataset(self) -> tensorflow.data.Dataset:
        if self._dataset is None:
            self.load()
        return self._dataset


def get_processing_f(vocab_table):
    tokenizer = tf_text.UnicodeScriptTokenizer()

    def preprocess_text(text, label=None):
        standardized = tf_text.case_fold_utf8(text)
        tokenized = tokenizer.tokenize(standardized)
        vectorized = vocab_table.lookup(tokenized)[0] # shape [1, None], because of dataset batch_size
        if label is None:
            return vectorized
        return (vectorized,
                tf.squeeze(label))
    return preprocess_text


def get_tokenize_f():
    tokenizer = tf_text.UnicodeScriptTokenizer()

    def tokenize(text, unused_label):
        lower_case = tf_text.case_fold_utf8(text)
        return tokenizer.tokenize(lower_case)
    return tokenize


def load_embeddings_from_file_with_vocab(fn, vocab_index):
    embeddings_index = {}
    embedding_dim = 0
    num_tokens = len(vocab_index) + 2

    with open(fn) as f:
        for line in f:
            word, coefs = line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embeddings_index[word] = coefs
            embedding_dim = len(coefs)

    # Prepare embedding matrix
    embedding_matrix = np.random.randn((num_tokens, embedding_dim))
    for word, i in vocab_index.items():
        embedding_vector = embeddings_index.get(word.decode('utf-8'))
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

    return embedding_matrix