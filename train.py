import argparse
import os
import tensorflow as tf

from config import Config
from dataset import DatasetLoader, get_tokenize_f, get_processing_f
from model import SimpleIntentDetectionModel, PredictWrapper, EmbeddingMonitor


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_path', type=str, default='data/processed')
    parser.add_argument('--output_file', type=str, default='trained_model')
    parser.add_argument('--output_embeddings', type=str, default='embedding_values')
    args = parser.parse_args()

    # load datasets
    print('Loading train dataset')
    train_dataset = DatasetLoader(dataset_path=os.path.join(args.dataset_path, 'train')).dataset
    print('Loading valid dataset')
    valid_dataset = DatasetLoader(dataset_path=os.path.join(args.dataset_path, 'valid')).dataset
    print('Loading test dataset')
    test_dataset = DatasetLoader(dataset_path=os.path.join(args.dataset_path, 'test')).dataset

    for example in train_dataset.take(1):
        print(example)

    # compute vocabulary
    print('Computing vocabulary...')
    vocab = set()
    all_dataset = train_dataset.concatenate(valid_dataset).map(get_tokenize_f())
    for tokens in all_dataset.as_numpy_iterator():
        # tokens is 2D array [batch,sentence]
        vocab.update({tk for sent_tokens in tokens for tk in sent_tokens})
    vocab = list(vocab)
    values = range(2, len(vocab) + 2)  # reserve 0, 1
    vocab_index = {key:val for key, val in zip(vocab, values)}
    init = tf.lookup.KeyValueTensorInitializer(
        vocab, values, key_dtype=tf.string, value_dtype=tf.int64)
    vocab_table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=1)

    processing_f = get_processing_f(vocab_table)
    train_dataset = train_dataset.map(processing_f).padded_batch(batch_size=Config.batch_size,
                                                                 padded_shapes=([None], # pad vector elements to longest
                                                                                [])) # do not pad labels
    valid_dataset = valid_dataset.map(processing_f).padded_batch(batch_size=Config.batch_size,
                                                                 padded_shapes=([None],
                                                                                []))
    for example in train_dataset.take(1):
        print(example)

    print('Training model...')
    if not os.path.isdir(args.output_embeddings):
        os.makedirs(args.output_embeddings)
    embedding_mon_cb = EmbeddingMonitor(output_dir=args.output_embeddings, vocab=vocab_index)
    model = SimpleIntentDetectionModel(embedding_dim=Config.embedding_dim,
                                       vocab_size=len(vocab)+2,
                                       num_targets=len(Config.known_intents))
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False),
                  metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])
    model.fit(train_dataset,
              validation_data=valid_dataset,
              epochs=Config.epochs,
              callbacks=[embedding_mon_cb])

    prediction_model = PredictWrapper(model, vocab_table)
    tf.saved_model.save(prediction_model, args.output_file)
