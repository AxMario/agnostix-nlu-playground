import sys
import tensorflow as tf
import tensorflow_text

import model

if __name__ == '__main__':
    should_stop = False
    model = tf.saved_model.load(sys.argv[1])
    while not should_stop:
        inp = input()
        if len(inp) > 0:
            prediction = model([[inp]])
            print(f'predicted intent index: {prediction}')
        else:
            should_stop = True
