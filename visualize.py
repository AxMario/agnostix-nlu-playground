import numpy as np
import plotly.graph_objs as go
from sklearn.decomposition import PCA
import sys

def display_pca_scatterplot_2D(vocab, user_input=None, words=None, label=None, color_map=None, topn=5, sample=2):
    if words == None:
        if sample > 0:
            words = np.random.choice(list(vocab.keys()), sample)
        else:
            words = [word for word in vocab]

    word_vectors = np.array([vocab[w] for w in words])

    # reduced = PCA(random_state=0).fit_transform(word_vectors)[:, :2]
    reduced = word_vectors

    data = []
    count = 0

    for i in range(len(user_input)):
        trace = go.Scatter(
            x=reduced[count:count + topn, 0],
            y=reduced[count:count + topn, 1],
            text=words[count:count + topn],
            name=user_input[i],
            textposition="top center",
            textfont_size=20,
            mode='markers+text',
            marker={
                'size': 10,
                'opacity': 0.8,
                'color': 2
            }

        )

        data.append(trace)
        count = count + topn

    trace_input = go.Scatter(
        x=reduced[count:, 0],
        y=reduced[count:, 1],
        text=words[count:],
        name='input words',
        textposition="top center",
        textfont_size=20,
        mode='markers+text',
        marker={
            'size': 10,
            'opacity': 1,
            'color': 'black'
        }
    )

    data.append(trace_input)

    # Configure the layout

    layout = go.Layout(
        margin={'l': 0, 'r': 0, 'b': 0, 't': 0},
        showlegend=True,
        legend=dict(
            x=1,
            y=0.5,
            font=dict(
                family="Courier New",
                size=25,
                color="black"
            )),
        font=dict(
            family=" Courier New ",
            size=15),
        autosize=False,
        width=1000,
        height=1000
    )

    plot_figure = go.Figure(data=data, layout=layout)
    plot_figure.show()


if __name__ == '__main__':
    dct = dict()
    with open(sys.argv[1]) as fd:
        for line in fd:
            line = line.split(' ')
            w = line [0]
            emb = [float(f) for f in line[1:]]
            dct[w] = emb
    display_pca_scatterplot_2D(vocab=dct, user_input=[],
                               words=['italian', 'chinese', 'english', 'cheap', 'expensive', 'bye', 'goodbye', 'thank', 'you', 'address', 'phone'])
