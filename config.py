class Config:
    batch_size = 4
    known_intents = ['inform', 'request', 'other']
    embedding_dim = 2
    epochs = 1