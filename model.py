import os.path

import tensorflow as tf
import tensorflow.keras.layers as tf_layers

from dataset import get_processing_f


class SimpleIntentDetectionModel(tf.keras.Model):
    def __init__(self, embedding_dim, vocab_size, num_targets):
        super(SimpleIntentDetectionModel, self).__init__()
        self.embedding = tf_layers.Embedding(input_dim=vocab_size,
                                             output_dim=embedding_dim,
                                             mask_zero=True, trainable=True)
        self.aggregate = tf_layers.Lambda(lambda x: tf.math.reduce_sum(x, axis=1)) # sum length dimension (=1)
        self.dense1 = tf_layers.Dense(50, activation='tanh')
        self.out_layer = tf_layers.Dense(num_targets, activation='linear')

    def __call__(self, inputs, *args, **kwargs):
        x = self.embedding(inputs)
        x = self.aggregate(x)
        x = self.dense1(x)
        x = self.out_layer(x)
        return tf.nn.softmax(x, axis=-1 )

    def get_config(self):
        return {"hidden_units": 50}


class PredictWrapper(tf.Module):
    def __init__(self, model, vocab_table):
        super(PredictWrapper, self).__init__()
        self._model = model
        self.vocab_table = vocab_table
        self.processing_f = get_processing_f(self.vocab_table)

    @tf.function(input_signature=[tf.TensorSpec([None, 1], tf.string)])
    def __call__(self, input_sentence):
        input_sentence = self.processing_f(input_sentence)
        predicted = self._model(input_sentence)
        return tf.argmax(predicted, axis=-1)


class EmbeddingMonitor(tf.keras.callbacks.Callback):
    def __init__(self, output_dir, vocab):
        super(EmbeddingMonitor, self).__init__()
        self.output_dir = output_dir
        self.vocab = vocab
        self.epoch = 0
        self.batch_no = 0

    def on_batch_end(self, batch, logs=None):
        if self.epoch == 0 and self.batch_no % 100 == 0:
            with open(os.path.join(self.output_dir, str(self.batch_no)), 'wt') as fd:
                weights = self.model.embedding.weights[0].numpy()
                for word, idx in self.vocab.items():
                    print(word.decode('utf-8'), ' '.join([str(w) for w in weights[idx]]), file=fd)
        self.batch_no += 1

    def on_epoch_end(self, epoch, logs=None):
        self.epoch += 1
        self.written = False