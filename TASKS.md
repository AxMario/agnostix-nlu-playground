# ASSIGNMENTS

 - Play with hyperparameters (epochs, batch_size, embedding_dim).
 - Try pretrained embeddings.
 - Add more intent categories (there's more than 3 in the data).
 - Write evaluation metrics yourself (F1-score, per-slot accuracy...)
   - compare performance of various model versions
 - Try monitoring the train and eval metrics, early stopping
 - Improve the predictions to yield intent names directly (not just indices).
 - Try monitoring more train/val metrics, use different loss function (e.g. CategoricalCrossEntropy, BinaryCrossEntropy, MSE)
   - This might require transforming the labels (1-hot vectors,...)
 - Modify the architecture - *reduce_mean, convolution, rnn, more layers*...
 - Achieve the best performance -- challenge? :-)
 - (advanced) Try processing the data to get complete NLU info (including slots and values) and train full NLU
 - (advanced) Implement model for the same task in *sklearn*.
   - logistic regression or SVM from bag-of-words/ bag-of-embeddings
   - compare
