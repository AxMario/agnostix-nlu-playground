import json
import os
import sys
import glob

from config import Config


def process_file(fn, target_dir):
    with open(fn, 'rt') as fd:
        data = json.load(fd)

    for n, item in enumerate(data):
        item_intent = item['DA'].split('(')[0].lower()
        if item_intent not in Config.known_intents:
            item_intent = 'other'
        with open(os.path.join(target_dir, item_intent, f'{n}.txt'), 'wt')as ofd:
            print(item['usr'], file=ofd, end='')


def make_dir_structure(dirname):
    for intent in Config.known_intents:
        os.makedirs(os.path.join(dirname, intent), exist_ok=True)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Provide directory to process!')
        sys.exit(1)
    dirname = sys.argv[1]
    if not os.path.isdir(dirname):
        print('Directory does not exist')
        sys.exit(1)

    for fn in glob.glob(os.path.join(dirname, '*.json')):
        if 'train' in fn:
            d = os.path.join(dirname, 'processed', 'train')
        elif 'dev' in fn:
            d = os.path.join(dirname, 'processed', 'valid')
        else:
            d = os.path.join(dirname, 'processed', 'test')
        make_dir_structure(d)
        process_file(fn, d)
